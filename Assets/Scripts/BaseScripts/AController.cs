﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace BaseScripts
{
    public class AController : MonoBehaviour
    {
        //PlayerControls Controls;

        private PlayerInput _playerInput;
        private APawn _privatePawn = null;

        private void Awake()
        {
            _playerInput = GetComponent<PlayerInput>();
        }

        public void Possess(APawn newPawn)
        {
            if (_privatePawn)
            {
                _privatePawn.SetNewController(null);
                _privatePawn = null;
            }
            
            _privatePawn = newPawn;
            if (_privatePawn)
            {
                _privatePawn.SetNewController(this);
            }
        }

        public void Unpossess(APawn oldPawn)
        {
            _privatePawn = null;
            oldPawn.SetNewController(null);
        }

        protected PlayerInput GetPlayerInput()
        {
            return _playerInput;
        }

        protected int GetPlayerIndex()
        {
            return _playerInput.playerIndex;
        }

        protected APawn GetPawn()
        {
            return _privatePawn;
        }
    }
}