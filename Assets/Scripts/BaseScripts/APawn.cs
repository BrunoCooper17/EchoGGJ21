﻿using UnityEngine;

namespace BaseScripts
{
    public class APawn : MonoBehaviour
    {
        private AController _controller;

        public AController GetController()
        {
            return _controller;
        }

        public void SetNewController(AController newController)
        {
            _controller = newController;
        }

        public bool IsPossessed()
        {
            return _controller != null;
        }
    }
}