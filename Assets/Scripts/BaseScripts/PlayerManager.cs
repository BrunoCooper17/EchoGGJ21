﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace BaseScripts
{
    public class PlayerManager : MonoBehaviour
    {
        private void OnPlayerLeft(PlayerInput obj)
        {
            Debug.Log("Player left " + obj.name);
        }

        private void OnPlayerJoined(PlayerInput obj)
        {
            Debug.Log("Player joined " + obj.name);
        }

        private void OnDisable()
        {
            ;
        }

        private void OnEnable()
        {
            ;
        }
    }
}