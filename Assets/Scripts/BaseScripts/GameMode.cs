﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Linq;
using Helpers;
using Misc;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.InputSystem;
using Debug = UnityEngine.Debug;

namespace BaseScripts
{
    public class GameMode : MonoBehaviour
    {
        private enum EGameState
        {
            Start,
            Loop,
            Win,
            Dead
        };

        [SerializeField] private float timeCameraStatic = 3;
        [SerializeField] private ChangeSceneScript changeScene;
        [SerializeField] private Canvas tryAgainCanvas;

        private static EGameState _gameState;

        private APawn[] _pawnList;
        private CameraMovement _cameraMovement;
        private PlayerInputManager _playerInputManager;

        private void Awake()
        {
            _pawnList = FindActorsHelper.GetPawnsInScene();
            _cameraMovement = FindObjectOfType<CameraMovement>();
            _playerInputManager = FindObjectOfType<PlayerInputManager>();

            foreach (var aPawn in _pawnList)
            {
                var pawn = (PawnBat) aPawn;
                pawn.EventPawnDead += OnPawnDead;
                pawn.EventPawnWin += OnPawnWin;
            }

            _cameraMovement.enabled = false;
            _gameState = EGameState.Start;
        }

        private void FixedUpdate()
        {
            switch (_gameState)
            {
                case EGameState.Start:
                    if (_playerInputManager.playerCount > 0)
                    {
                        StartCoroutine(StartCameraMoving(timeCameraStatic));
                        _gameState = EGameState.Loop;
                    }
                    break;
        
                case EGameState.Loop:
                    break;
        
                case EGameState.Win:
                    break;
        
                case EGameState.Dead:
                    break;
        
        
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IEnumerator StartCameraMoving(float timeToWait)
        {
            yield return new WaitForSeconds(timeToWait);
            _cameraMovement.enabled = true;
        }

        private void OnPawnDead(APawn pawn)
        {
            Debug.Log("You're Dead! " + pawn.gameObject.name);
            foreach (var tmpPawn in _pawnList)
            {
                if(tmpPawn != pawn)
                {
                    ((PawnBat) tmpPawn).StopPawnFromMoving(Vector2.zero);
                }

                var controller = tmpPawn.GetController();
                if (controller)
                {
                    controller.Unpossess(tmpPawn);
                }
            }

            ((PawnBat) pawn).StopPawnFromMoving(Vector2.down);
            _cameraMovement.enabled = false;
            _gameState = EGameState.Dead;

            var tmpTryAgainCanvas = Instantiate(tryAgainCanvas, FindObjectOfType<Camera>().transform);
            tmpTryAgainCanvas.transform.position += Vector3.forward * 5;
            StartCoroutine(GoToScreen(0.0f, "MainMenu", 2.5f));
        }

        private void OnPawnWin(APawn pawn)
        {
            var controller = pawn.GetController();
            ((PawnBat)pawn).StopPawnFromMoving(Vector2.up * 1.5f);
            pawn.GetController().Unpossess(pawn);
            pawn.enabled = false;
            
            var pawns = FindActorsHelper.GetPawnsUnpossessed();
            if (pawns.Length > 0)
            {
                controller.Possess(pawns[0]);
            }
            
            var bWin = _pawnList.All(tmpPawn => ((PawnBat) tmpPawn).Win);
            if (!bWin) return;

            Debug.Log("You're winner!");
            _cameraMovement.enabled = false;
            _gameState = EGameState.Win;

            StartCoroutine(GoToScreen(1.5f, "Ending", 0.55f));
        }

        private IEnumerator GoToScreen(float timeToWait, string newScene, float timeToWaitScreen)
        {
            yield return new WaitForSeconds(timeToWait);
            var tmpChangeScene = Instantiate(changeScene, FindObjectOfType<Camera>().transform);
            tmpChangeScene.transform.position += Vector3.forward * 5;
            tmpChangeScene.SetScreenToTransition(newScene);
            yield return tmpChangeScene.ChangeScene(timeToWaitScreen);
        }

        public static bool IsGameOver()
        {
            return _gameState == EGameState.Dead;
        }
    }
}