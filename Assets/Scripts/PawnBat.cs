﻿// using System.Collections;
// using System.Collections.Generic;

using System;
using BaseScripts;
using Helpers;
using Misc;
using Sounds;
using UnityEngine;

//using UnityEngine.Serialization;

public class PawnBat : APawn
{
    [SerializeField] private float echoDistance = 2.75f;
    [SerializeField] private EcholocationBehaviour echoPrefab;
    [SerializeField] private Cooldown echoCooldownPrefab;
    [SerializeField] private Cooldown echoAnimCooldownPrefab;
    [SerializeField] private Cooldown damageCooldownPrefab;

    public delegate void DelegatePawnVoid(APawn pawn);

    public DelegatePawnVoid EventPawnDead;
    public DelegatePawnVoid EventPawnWin;

    private const string AnimDamage = "Damage";
    private const string AnimIdle = "Idle";
    private const string AnimUp = "Up";
    private const string AnimDown = "Down";
    private const string AnimEchoIdle = "EchoIdle";
    private const string AnimEchoUp = "EchoUp";
    private const string AnimEchoDown = "EchoDown";

    private Animator _animator;
    private AudioSource _audioDamage;
    private CircleCollider2D _collision;
    private Cooldown _damageCooldown;
    private Cooldown _echoCooldown;
    private Cooldown _echoAnimCooldown;
    private EchoSounds _echoAudioClips;
    private LightFlicker _lightFlicker;
    private MoveController _moveController;
    private SpriteRenderer _batSpriteRenderer;  
    private WingSounds _wingsAudioClips;


    private bool _bEchoAnim;
    private string _currentAnimPlaying = "";
    private float _offsetX;

    public bool Win { get; private set; }

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _audioDamage = GetComponent<AudioSource>();
        _collision = GetComponent<CircleCollider2D>(); 
        _damageCooldown = Instantiate(damageCooldownPrefab);
        _echoCooldown = Instantiate(echoCooldownPrefab);
        _echoAnimCooldown = Instantiate(echoAnimCooldownPrefab);
        _echoAudioClips = GetComponentInChildren<EchoSounds>();
        _lightFlicker = GetComponent<LightFlicker>();
        _moveController = GetComponent<MoveController>();
        _batSpriteRenderer = GetComponent<SpriteRenderer>();
        _wingsAudioClips = GetComponentInChildren<WingSounds>();

        _damageCooldown.CooldownEnded += OnDamageEnded;
        _echoAnimCooldown.CooldownEnded += OnEchoAnimEnded;

        _offsetX = _collision.offset.x;
    }

    private void Update()
    {
        UpdateAnimation();

        if (_echoCooldown)
        {
            _lightFlicker.enabled = !_echoCooldown.enabled;
        }
    }

    public void SpawnEcholocation()
    {
        if (_damageCooldown.enabled)
        {
            return;
        }

        if (_echoCooldown.enabled)
        {
            return;
        }

        _echoCooldown.ActivateCooldown();
        _echoAnimCooldown.ActivateCooldown();
        _bEchoAnim = true;
        
        _echoAudioClips.PlayRandomClip();

        if (!echoPrefab) return;
        var tmpEcho = Instantiate(echoPrefab);
        Vector3 tmpDirection = _moveController.GetLastDirection();
        tmpEcho.transform.position = transform.position + (tmpDirection * echoDistance);
    }

    public void MovePawn(Vector2 direction)
    {
        if (_damageCooldown.enabled)
        {
            return;
        }

        _moveController.Move(direction);

        var collisionOffset = _collision.offset;
        if (direction.x < 0)
        {
            _batSpriteRenderer.flipX = true;
            collisionOffset.x = _offsetX * -1;
        }
        else if (direction.x > 0)
        {
            _batSpriteRenderer.flipX = false;
            collisionOffset.x = _offsetX;
        }

        _collision.offset = collisionOffset;
    }

    private void UpdateAnimation()
    {
        if (_damageCooldown.enabled)
        {
            return;
        }

        string animToPlay;
        var batDirection = DirectionHelper.Vector2ToDirection(_moveController.GetCurrentMoveDirection());
        switch (batDirection)
        {
            case EDirection.Left:
            case EDirection.Right:
            case EDirection.None:
                animToPlay = _bEchoAnim ? AnimEchoIdle : AnimIdle;
                break;

            case EDirection.Up:
                animToPlay = _bEchoAnim ? AnimEchoUp : AnimUp;
                break;

            case EDirection.Down:
                animToPlay = _bEchoAnim ? AnimEchoDown : AnimDown;
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }

        if (_currentAnimPlaying == animToPlay)
        {
            return;
        }

        _currentAnimPlaying = animToPlay;
        _animator.Play(animToPlay, 0, _animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        switch ((ECollisionProfiles) collision.gameObject.layer)
        {
            case ECollisionProfiles.Spkies:
                _audioDamage.Play();
                _damageCooldown.ActivateCooldown();
                Vector3 tmpLastOrientation = _moveController.GetCurrentMoveDirection().normalized * -30;
                _moveController.MoveDamage(tmpLastOrientation);
                _moveController.Move(Vector2.zero);

                _animator.Play(AnimDamage, 0, _animator.GetCurrentAnimatorStateInfo(0).normalizedTime % 1);
                break;

            case ECollisionProfiles.Goal:
                Win = true;
                EventPawnWin?.Invoke(this);
                break;

            case ECollisionProfiles.Killscreen:
                EventPawnDead?.Invoke(this);
                _collision.enabled = false;
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void StopPawnFromMoving(Vector2 lastDirection)
    {
        _moveController.LastMove(lastDirection);
        _moveController.enabled = false;
    }
    
    public void CancelPawnMove()
    {
        _moveController.LastMove(Vector2.zero);
    }

    private void OnDamageEnded()
    {
        _animator.Play(AnimIdle, 0, 0);
    }

    private void OnEchoAnimEnded()
    {
        _bEchoAnim = false;
    }

    private void PlayWings()
    {
        _wingsAudioClips.PlayRandomClip();
    }
}