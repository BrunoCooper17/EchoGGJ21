﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Cooldown : MonoBehaviour
{
    [SerializeField] private float timeCooldown = 5.0f;

    private float _timeElapse;

    public delegate void DelegateCooldownEnded();

    public DelegateCooldownEnded CooldownEnded;

    private void Awake()
    {
        // We don't allow Negative or 0 time for the duration of cooldowns.
        if (timeCooldown <= 0)
        {
            timeCooldown = 0.1f;
        }

        enabled = false;
    }

    // Update is called once per frame
    private void Update()
    {
        _timeElapse += Time.deltaTime;
        //Debug.Log("Cooldown? " + TimeElapse);

        if (!(_timeElapse >= timeCooldown))
        {
            return;
        }

        enabled = false;
        CooldownEnded?.Invoke();
    }

    public float GetPercentageDone()
    {
        return Mathf.Clamp(_timeElapse / timeCooldown, 0.0f, 1.0f);
    }

    public void ActivateCooldown()
    {
        _timeElapse = 0;
        enabled = true;
    }
}