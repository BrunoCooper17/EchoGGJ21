﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextosGameplayFire : MonoBehaviour
{
    public Animator animator;

    // Update is called once per frame
    public void FireTryAgainTexts()
    {
        animator.SetBool("FireTryAgainText" , true);
    }
}
