﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Serialization;

public class EcholocationBehaviour : MonoBehaviour
{
    [SerializeField] private float maxSize = 3.0f;
    [SerializeField] private float duration = 5.0f;
    [SerializeField] private AnimationCurve expandBehaviour;
    [SerializeField] private AnimationCurve lightIntensity;

    private Light2D _echoLight;
    private float _timeElapsed;

    private void Awake()
    {
        _echoLight = GetComponent<Light2D>();

        // We don't allow Negative or 0 time for the duration.
        if (duration <= 0)
        {
            duration = .1f;
        }
    }

    private void Update()
    {
        _timeElapsed += Time.deltaTime;

        _echoLight.pointLightOuterRadius = maxSize * expandBehaviour.Evaluate(_timeElapsed / duration);
        _echoLight.intensity = lightIntensity.Evaluate(_timeElapsed / duration);

        if (_timeElapsed >= duration)
        {
            Destroy(gameObject);
        }
    }
}