﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextosAnimationFire : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator animator;

    // Update is called once per frame
    public void FireTextAnimation()
    {
        
        animator.SetBool("FireTextos" , true);
    }
}
