﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Helpers
{
    public enum EDirection
    {
        None,
        Up,
        Down,
        Left,
        Right
    };

    public static class DirectionHelper
    {
        public static EDirection Vector2ToDirection(Vector2 direction)
        {
            if (direction == Vector2.zero)
            {
                return EDirection.None;
            }

            if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
            {
                return direction.x < 0 ? EDirection.Left : EDirection.Right;
            }
            else
            {
                return direction.y < 0 ? EDirection.Down : EDirection.Up;
            }
        }
    }
}