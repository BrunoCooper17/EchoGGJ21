﻿using System;
using System.Collections.Generic;
using System.Linq;
using BaseScripts;
using UnityEngine;

namespace Helpers
{
    public class FindActorsHelper : MonoBehaviour
    {
        public static APawn[] GetPawnsInScene()
        {
            var tmpPawns = FindObjectsOfType<APawn>();
            Array.Reverse(tmpPawns);
            return tmpPawns;
        }

        public static APawn[] GetPawnsUnpossessed()
        {
            var tmpPawns = GetPawnsInScene();
            return tmpPawns.Where(tmpPawn => !tmpPawn.IsPossessed() && tmpPawn.enabled).ToArray();
        }

        public static APawn[] GetPawnsPossessed()
        {
            var tmpPawns = GetPawnsInScene();
            return tmpPawns.Where(tmpPawn => tmpPawn.IsPossessed()).ToArray();
        }
    }
}