﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MoveController : MonoBehaviour
{
    [SerializeField] private float velocity = 150;
    [SerializeField] private AnimationCurve velocityCurve;

    private Animator _animator;

    private Vector3 _currentDirection;
    private Vector2 _currentDirection2;
    private Rigidbody2D _rigidbody;

    private Vector2 _lastDirection;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        _rigidbody.velocity = _currentDirection * (velocity * Time.fixedDeltaTime *
                                                   velocityCurve.Evaluate(_animator.GetCurrentAnimatorStateInfo(0)
                                                       .normalizedTime));
    }

    public Vector2 GetCurrentMoveDirection()
    {
        return _currentDirection2;
    }

    public void Move(Vector2 direction)
    {
        //Debug.Log("DIRECTION TO MOVE: " + Direction);
        if (!enabled) return;

        _currentDirection2.x = direction.x;
        _currentDirection2.y = direction.y;

        _currentDirection.x = direction.x;
        _currentDirection.y = direction.y;

        if (direction != Vector2.zero)
        {
            _lastDirection = direction;
        }
    }

    public void MoveDamage(Vector3 direction)
    {
        _rigidbody.MovePosition(transform.position + direction * Time.fixedDeltaTime *
            velocityCurve.Evaluate(_animator.GetCurrentAnimatorStateInfo(0).normalizedTime));
    }

    public void LastMove(Vector2 direction)
    {
        Move(direction);
        _rigidbody.velocity = direction * (velocity * Time.fixedDeltaTime);
    }

    public Vector2 GetLastDirection()
    {
        return _lastDirection.normalized;
    }

}