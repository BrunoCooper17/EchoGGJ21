﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace Sounds
{
    public class RandomAudioClip : MonoBehaviour
    {
        [SerializeField] private AudioClip[] randomAudio;

        private AudioSource _audioSource;

        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
        }

        public void PlayRandomClip()
        {
            if (!_audioSource) return;
            _audioSource.clip = randomAudio[Random.Range(0, randomAudio.Length)];
            _audioSource.Play();
        }
    }
}