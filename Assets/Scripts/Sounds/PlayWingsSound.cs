﻿using System;
using Sounds;
using UnityEngine;

namespace Sounds
{
    public class PlayWingsSound : MonoBehaviour
    {
        private WingSounds _wingsAudioClips;
        private void Awake()
        {
            _wingsAudioClips = GetComponentInChildren<WingSounds>();
        }

        private void PlayWings()
        {
            _wingsAudioClips.PlayRandomClip();
        }
    }
}