﻿using System.Collections;
using System.Collections.Generic;
using BaseScripts;
using Helpers;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : AController
{
    private void Start()
    {
        if (GameMode.IsGameOver()) return;
        
        var pawns = FindActorsHelper.GetPawnsUnpossessed();
        if (pawns.Length > 0)
        {
            Possess(pawns[0]);
        }
    }

    private void OnMovement(InputValue axis)
    {
        var axisValue = axis.Get<Vector2>();
        var pawnBat = (PawnBat) GetPawn();
        if (pawnBat)
        {
            pawnBat.MovePawn(axisValue);
        }
    }

    private void OnEcho()
    {
        var pawnBat = (PawnBat) GetPawn();
        if (pawnBat)
        {
            pawnBat.SpawnEcholocation();
        }
    }

    private void OnDisconnect()
    {
        Debug.Log("PlayerIndex: " + GetPlayerIndex() + " Disconnect :(");
        Destroy(gameObject, 1);
    }

    private void OnChangePawn()
    {
        if (!GetPawn()) return;

        var availablePawns = FindActorsHelper.GetPawnsUnpossessed();
        if (availablePawns.Length <= 0) return;
        
        ((PawnBat)GetPawn()).CancelPawnMove();
        Possess(availablePawns[0]);
    }
}