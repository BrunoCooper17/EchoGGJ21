﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Experimental.Rendering.Universal;

public class MenuLIghtSize : MonoBehaviour
{
    [SerializeField] private AnimationCurve lightSize;

    private Light2D _light2D;
    private float _sizeOuterBase;
    private float _sizeInnerBase;
    private float _timeAcum;
    private void Awake()
    {
        _light2D = GetComponent<Light2D>();
        _sizeOuterBase = _light2D.pointLightOuterRadius;
        _sizeInnerBase = _light2D.pointLightInnerRadius;
    }
    
    private void Update()
    {
        _timeAcum += Time.deltaTime;
        _light2D.pointLightOuterRadius = _sizeOuterBase * lightSize.Evaluate(_timeAcum);
        _light2D.pointLightInnerRadius = _sizeInnerBase * lightSize.Evaluate(_timeAcum);
    }
}
