﻿using UnityEngine;

namespace Misc
{
    public class DestroyObject : MonoBehaviour
    {
        public void DestroyEvent()
        {
            Destroy(gameObject);
        }
    }
}
