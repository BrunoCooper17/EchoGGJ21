﻿using BaseScripts;
using UnityEngine;

namespace Misc
{
    public class DirectorPawns : MonoBehaviour
    {
        [SerializeField] private float pawnVelocityFactor = 1.0f;
        
        private APawn[] _pawnBats;
        private void Start()
        {
            _pawnBats = Helpers.FindActorsHelper.GetPawnsInScene();

            foreach (var pawnBat in _pawnBats)
            {
                ((PawnBat)pawnBat).EventPawnWin += OnPausePawn;
            }
        }

        // Update is called once per frame
        private void Update()
        {
            foreach (var pawnBat in _pawnBats)
            {
                ((PawnBat)pawnBat).MovePawn(Vector2.up * pawnVelocityFactor);
            }
        }

        private void OnPausePawn(APawn pawn)
        {
            ((PawnBat)pawn).StopPawnFromMoving(Vector2.zero);
        }

        private void PlayEcholocalization()
        {
            foreach (var pawnBat in _pawnBats)
            {
                ((PawnBat)pawnBat).SpawnEcholocation();
            }
        }
    }
}
