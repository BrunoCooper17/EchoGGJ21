﻿using System.Collections;
using UnityEngine;

namespace Misc
{
    public class MainMenuChange : MonoBehaviour
    {
        [SerializeField] private ChangeSceneScript changeScene;

        public void GoToLevel(string levelToLoad)
        {
            StartCoroutine(GoToLevelCoroutine(levelToLoad));
        }
    
        private IEnumerator GoToLevelCoroutine(string levelToLoad)
        {
            var tmpChangeScene = Instantiate(changeScene, FindObjectOfType<Camera>().transform);
            tmpChangeScene.transform.position += Vector3.forward * 5;
            tmpChangeScene.SetScreenToTransition(levelToLoad);
            yield return tmpChangeScene.ChangeScene(0.6f);
        }

        public void CloseAwesomeGame()
        {
            Application.Quit();
        }
    }
}
