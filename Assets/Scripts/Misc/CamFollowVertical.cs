﻿using BaseScripts;
using UnityEngine;

namespace Misc
{
    public class CamFollowVertical : MonoBehaviour
    {
        [SerializeField] protected APawn pawnToFollow;

        // Update is called once per frame
        private void Update()
        {
            var gameObjectTransform = transform;
            var transformPosition = gameObjectTransform.position;
        
            transformPosition.y = pawnToFollow.transform.position.y;
            gameObjectTransform.position = transformPosition;
        }
    }
}
