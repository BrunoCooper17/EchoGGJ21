﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Misc
{
    public class ChangeSceneScript : MonoBehaviour
    {
        [SerializeField] private string newScreen;

        public IEnumerator ChangeScene(float timeToWait)
        {
            yield return new WaitForSeconds(timeToWait);
            SceneManager.LoadScene(newScreen);
        }

        public void SetScreenToTransition(string newScene)
        {
            newScreen = newScene;
        }
    }
}