﻿using System;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace Misc
{
    public class LightFlicker : MonoBehaviour
    {
        [SerializeField] private AnimationCurve lightCurve;

        private float _timeAcum;
        private Light2D _light;
        private float _baseIntensity;

        private void Awake()
        {
            _light = GetComponent<Light2D>();
            _baseIntensity = _light.intensity;
        }

        private void Update()
        {
            _timeAcum += Time.deltaTime;
            _light.intensity = _baseIntensity * lightCurve.Evaluate(_timeAcum);
        }

        private void OnDisable()
        {
            _light.intensity = _baseIntensity;
        }
    }
}