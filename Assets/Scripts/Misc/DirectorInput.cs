﻿using System.Collections;
using UnityEngine;

namespace Misc
{
    public class DirectorInput : MonoBehaviour
    {
        [SerializeField] private ChangeSceneScript changeScene;
        private void Update()
        {
            if (!Input.anyKey) return;
            StartCoroutine(GoToLevelCoroutine("MainMenu"));
        }
        
        private IEnumerator GoToLevelCoroutine(string levelToLoad)
        {
            var tmpChangeScene = Instantiate(changeScene, FindObjectOfType<Camera>().transform);
            tmpChangeScene.transform.position += Vector3.forward * 5;
            tmpChangeScene.SetScreenToTransition(levelToLoad);
            yield return tmpChangeScene.ChangeScene(0.6f);
        }
    }
}
