﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Vector3 velocity = new Vector3(0, 0.4f, 0);

    private void Awake()
    {
        gameObject.layer = (int) ECollisionProfiles.Killscreen;
    }

    private void Update()
    {
        transform.position += Time.deltaTime * velocity;
    }
}